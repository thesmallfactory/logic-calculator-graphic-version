package com.logicienne.partie.graphique;
/*
 * Nom de la classe : CouleurPourCellule
 *
 * Description		: Par cette classe, l'auteur peut red�finir le rendu des cellules de la table de v�rit�
 * 					  (de l'expression)...
 *
 * Version			: 1.0
 *
 * Cr�ation			: du 21/09/2015 au 21/10/2015
 *
 * Copyright		: Cyril Marilier
 */
import java.awt.Color;
import java.awt.Component;

import javax.swing.JTable;

import javax.swing.table.DefaultTableCellRenderer;

import com.logicienne.partie.intelligente.Applicatif;

public class CouleurPourCellule extends DefaultTableCellRenderer
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public Component getTableCellRendererComponent(JTable table, Object valeur, boolean isSelected, boolean hasFocus, int ligne, int colonne) {
		Component cellule = super.getTableCellRendererComponent(table, valeur, isSelected, hasFocus, ligne, colonne);
		if (ligne == table.getRowCount() - 1)
			if (table.getValueAt(table.getRowCount() - 1, colonne).equals(String.valueOf(Applicatif.calculer_ordreColonnesDeVerite().size()))) {
				cellule.setBackground(Color.WHITE);
				cellule.setForeground(new Color(91, 123, 160));
			}
			else {
				cellule.setBackground(new Color(255, 94, 77));
				cellule.setForeground(Color.WHITE);
			}
		else
			if (table.getValueAt(table.getRowCount() - 1, colonne).equals(String.valueOf(Applicatif.calculer_ordreColonnesDeVerite().size()))) {
				cellule.setBackground(new Color(1, 45, 64));
				cellule.setForeground(Color.WHITE);
			}
			else {
				cellule.setBackground(Color.WHITE);
				cellule.setForeground(Color.BLACK);
			}
		return cellule;
	}
}