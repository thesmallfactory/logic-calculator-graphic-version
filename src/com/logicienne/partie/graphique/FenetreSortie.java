package com.logicienne.partie.graphique;
/*
 * Nom de la classe : FenetreSortie
 *
 * Description		: Cette fen�tre "affiche" la table de v�rit� de l'expression ("bien form�e")
 * 					  et permet, � l'utilisateur, de l'imprimer.
 *
 * Version			: 1.0
 *
 * Cr�ation			: du 21/09/2015 au 21/10/2015
 *
 * Copyright		: Cyril Marilier
 */
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Toolkit;

import javax.swing.BorderFactory;
import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.LayoutStyle;

import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import javax.swing.table.DefaultTableCellRenderer;

import com.logicienne.entites.Atome;

import com.logicienne.enumerations.Atom;
import com.logicienne.enumerations.Operator;

import com.logicienne.partie.intelligente.Applicatif;

public class FenetreSortie extends JFrame implements ListSelectionListener
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

/*****************************************
 * D�claration des variables de contr�le :
 *****************************************/
	// Panneau de la table de v�rit� (de l'expression) :
	/* Table de num�rotation des lignes :*/
	private TableNumerotationLignes numerotationLignes_table;
	private JScrollPane numerotationLignes_volet;
	/* Tables de v�rit� des atomes :*/
	private TableAtomes tablesDeVeriteAtomes_jTable;
	private JScrollPane tablesDeVeriteAtomes_volet;
	/* Table de v�rit� de l'expression :*/
	private JTable tableDeVeriteExpression_jTable;
	private JScrollPane tableDeVeriteExpression_volet;

	private JPanel tableDeVerite_panneau;

	// Bouton d'impression de la table de v�rit� (de l'expression) :
	private JButton imprimerTableDeVerite_bouton;

	// Panneau global de la fen�tre :
	private JPanel global_jPanel;

/********************************
 * Constructeur de la "fen�tre" :
 ********************************/
	public FenetreSortie(String copieExpression) {
		// Panneau de la table de v�rit� (de l'expression) :
	/* Table de num�rotation des lignes :*/
		String[][] numerotationLignes = new String[Applicatif.nombreLignes_tableDeVerite(Applicatif.nombreDeMasse())][1];
		for (int i = 0; i < Applicatif.nombreLignes_tableDeVerite(Applicatif.nombreDeMasse()); i++)
			numerotationLignes[i][0] = String.valueOf(i + 1);
		String[] NumeroPremiereLigne = {"0"};
		numerotationLignes_table = new TableNumerotationLignes(numerotationLignes, NumeroPremiereLigne);
		numerotationLignes_table.setBorder(BorderFactory.createLineBorder(new Color(1, 45, 64), 1));
		numerotationLignes_table.getTableHeader().setFont(new Font("Courier New", 1, 12));
		DefaultTableCellRenderer rendu = new DefaultTableCellRenderer();
		rendu.setHorizontalAlignment(JLabel.CENTER);
		rendu.setVerticalAlignment(JLabel.CENTER);
		numerotationLignes_table.getTableHeader().setDefaultRenderer(rendu);
		numerotationLignes_table.getColumnModel().getColumn(0).setCellRenderer(rendu);
		numerotationLignes_table.getTableHeader().setPreferredSize(new Dimension(16, 20));
		numerotationLignes_table.getTableHeader().setReorderingAllowed(false);
		numerotationLignes_table.getTableHeader().setResizingAllowed(false);
		numerotationLignes_table.setForeground(new Color(255, 94, 77));
		numerotationLignes_table.setFont(new Font("Courier New", 1, 12));
		numerotationLignes_table.getSelectionModel().addListSelectionListener(this);
		for (int i = 0; i < numerotationLignes_table.getRowCount(); i++)
			numerotationLignes_table.isCellEditable(i, 0);
		numerotationLignes_volet = new JScrollPane(numerotationLignes_table);
	/* Tables de v�rit� des atomes :*/
		String[][] tablesDeVeriteAtomes = Applicatif.tablesDeVerite_atomes(copieExpression);
		String[] nomsAtomes = new String[Atome.sExplicites().size()];
		int k = 0;
		for (Atom atome : Atom.values())
			if (copieExpression.contains(atome.getLettre()))
				nomsAtomes[k++] = atome.getLettre();
		tablesDeVeriteAtomes_jTable = new TableAtomes(tablesDeVeriteAtomes, nomsAtomes);
		tablesDeVeriteAtomes_jTable.setBorder(BorderFactory.createLineBorder(Color.BLACK, 1));
		tablesDeVeriteAtomes_jTable.setShowVerticalLines(true);
		tablesDeVeriteAtomes_jTable.getTableHeader().setFont(new Font("Courier New", 1, 12));
		((DefaultTableCellRenderer)tablesDeVeriteAtomes_jTable.getTableHeader().getDefaultRenderer()).setHorizontalAlignment(JLabel.CENTER);
		for (int i = 0; i < tablesDeVeriteAtomes_jTable.getColumnCount(); i++)
			tablesDeVeriteAtomes_jTable.getColumnModel().getColumn(i).setCellRenderer(rendu);
		tablesDeVeriteAtomes_jTable.getTableHeader().setReorderingAllowed(false);
		tablesDeVeriteAtomes_jTable.getTableHeader().setResizingAllowed(false);
		tablesDeVeriteAtomes_jTable.setFont(new Font("Courier New", 1, 12));
		tablesDeVeriteAtomes_jTable.setEnabled(false);
		tablesDeVeriteAtomes_jTable.setToolTipText("<html>"
													 + "<font style='font-family:Times New Roman;font-size:12px;font-style:italic;color:WHITE'>"
													 	 + "&nbsp;&nbsp;On convient que l�expression contient tous les atomes jusqu�� � <font color='#FF7F00'>"
													 	 + Atom.values()[Applicatif.numeroAtomique((byte)(Atome.sExplicites().size() - 1)) - 1].getLettre() + "</font> �...&nbsp;&nbsp;"
													 + "</font><br/>"
													 + "<font style='font-size:1px'>"
													 	 + "&nbsp;"
													 + "</font>"
												 + "</html>");
		tablesDeVeriteAtomes_volet = new JScrollPane(tablesDeVeriteAtomes_jTable);
	/* Table de v�rit� de l'expression :*/
		String[][] tableDeVerite = Applicatif.tableDeVerite(copieExpression);
		String regexSignesDesConnecteurs = "";
			for (Operator connecteur : Operator.values())
				regexSignesDesConnecteurs += connecteur.getSigne();
		String[] nomExpression = new String[copieExpression.replaceAll("[^" + regexSignesDesConnecteurs + "]+", " ").length()];
		int o = 0;
		int n = 0;
		int m = 0;
		for (; o < copieExpression.length(); o++)
			if (Character.toString(copieExpression.charAt(o)).matches("^[" + regexSignesDesConnecteurs + "]$")) {
				if (o != n)
					nomExpression[m++] = copieExpression.substring(n, o);
				nomExpression[m++] = Character.toString(copieExpression.charAt(o));
				n = o + 1;
			}
		nomExpression[m] = copieExpression.substring(n, o);
		tableDeVeriteExpression_jTable = new JTable(tableDeVerite, nomExpression);
		tableDeVeriteExpression_jTable.setBorder(BorderFactory.createLineBorder(Color.BLACK, 1));
		tableDeVeriteExpression_jTable.getTableHeader().setFont(new Font("Courier New", 1, 12));
		tableDeVeriteExpression_jTable.getTableHeader().setDefaultRenderer(rendu);
		CouleurPourCellule couleur = new CouleurPourCellule();
		couleur.setHorizontalAlignment(JLabel.CENTER);
		couleur.setVerticalAlignment(JLabel.CENTER);
		for (int i = 0; i < tableDeVeriteExpression_jTable.getColumnCount(); i++)
			tableDeVeriteExpression_jTable.getColumnModel().getColumn(i).setCellRenderer(couleur);
		for (int j = 0; j < nomExpression.length; j++)
			if (nomExpression[j].length() > 1) {
				tableDeVeriteExpression_jTable.getTableHeader().setPreferredSize(new Dimension(nomExpression[j].length() * 16, 20));
				tableDeVeriteExpression_jTable.getColumnModel().getColumn(j).setPreferredWidth(nomExpression[j].length() * 16);
			}
			else {
				tableDeVeriteExpression_jTable.getTableHeader().setPreferredSize(new Dimension(nomExpression[j].length() * 32, 20));
				tableDeVeriteExpression_jTable.getColumnModel().getColumn(j).setPreferredWidth(nomExpression[j].length() * 32);
			}
		tableDeVeriteExpression_jTable.getTableHeader().setReorderingAllowed(false);
		tableDeVeriteExpression_jTable.getTableHeader().setResizingAllowed(false);
		tableDeVeriteExpression_jTable.setFont(new Font("Courier New", 1, 12));
		tableDeVeriteExpression_jTable.setEnabled(false);
		tableDeVeriteExpression_volet = new JScrollPane(tableDeVeriteExpression_jTable);

		tableDeVerite_panneau = new JPanel();
		tableDeVerite_panneau.setBorder(BorderFactory.createLineBorder(new Color(1, 45, 64), 2));
		tableDeVerite_panneau.setBackground(Color.WHITE);

		GroupLayout tableDeVerite_layout = new GroupLayout(tableDeVerite_panneau);
		tableDeVerite_panneau.setLayout(tableDeVerite_layout);
		tableDeVerite_layout.setHorizontalGroup(tableDeVerite_layout.createParallelGroup(GroupLayout.Alignment.LEADING)
			.addGroup(tableDeVerite_layout.createSequentialGroup()
				.addContainerGap()
				.addComponent(numerotationLignes_volet, GroupLayout.PREFERRED_SIZE, 32, GroupLayout.PREFERRED_SIZE)
				.addComponent(tablesDeVeriteAtomes_volet, GroupLayout.PREFERRED_SIZE, nomsAtomes.length * 24, GroupLayout.PREFERRED_SIZE)
				.addPreferredGap(LayoutStyle.ComponentPlacement.RELATED, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
				.addComponent(tableDeVeriteExpression_volet, GroupLayout.PREFERRED_SIZE, nomExpression.length * 27, GroupLayout.PREFERRED_SIZE)
				.addContainerGap())
		);
		tableDeVerite_layout.setVerticalGroup(tableDeVerite_layout.createParallelGroup(GroupLayout.Alignment.LEADING)
			.addGroup(GroupLayout.Alignment.TRAILING, tableDeVerite_layout.createSequentialGroup()
				.addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
				.addGroup(tableDeVerite_layout.createParallelGroup(GroupLayout.Alignment.LEADING)
					.addComponent(numerotationLignes_volet, GroupLayout.PREFERRED_SIZE, 24 + numerotationLignes.length * 16, 26 + numerotationLignes.length * 16)
					.addComponent(tablesDeVeriteAtomes_volet, GroupLayout.PREFERRED_SIZE, 24 + tablesDeVeriteAtomes.length * 16, 26 + tablesDeVeriteAtomes.length * 16)
					.addComponent(tableDeVeriteExpression_volet, GroupLayout.PREFERRED_SIZE, 26 + tableDeVerite.length * 16, GroupLayout.PREFERRED_SIZE))
				.addContainerGap())
		);

		// Bouton d'impression de la table de v�rit� (de l'expression) :this.setBorder(null);
		imprimerTableDeVerite_bouton = new Bouton(Color.WHITE, new Color(1, 45, 64), "Imprimer", null);
		imprimerTableDeVerite_bouton.setFont(new Font("Courier New", 1, 21));
		imprimerTableDeVerite_bouton.addActionListener(new PanneauPourImpression(tableDeVerite_panneau));

		// Panneau global de la fen�tre :
		global_jPanel = new JPanel();
		global_jPanel.setBorder(BorderFactory.createLineBorder(new Color(1, 45, 64), 2));
		global_jPanel.setBackground(new Color(91, 123, 160));

		GroupLayout panneauGlobal_layout = new GroupLayout(global_jPanel);
		global_jPanel.setLayout(panneauGlobal_layout);
		panneauGlobal_layout.setHorizontalGroup(panneauGlobal_layout.createParallelGroup(GroupLayout.Alignment.LEADING)
			.addGroup(GroupLayout.Alignment.TRAILING, panneauGlobal_layout.createSequentialGroup()
				.addContainerGap()
				.addGroup(panneauGlobal_layout.createParallelGroup(GroupLayout.Alignment.TRAILING)
					.addComponent(imprimerTableDeVerite_bouton, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
					.addComponent(tableDeVerite_panneau, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
				.addContainerGap())
		);
		panneauGlobal_layout.setVerticalGroup(panneauGlobal_layout.createParallelGroup(GroupLayout.Alignment.LEADING)
			.addGroup(panneauGlobal_layout.createSequentialGroup()
				.addContainerGap()
				.addComponent(tableDeVerite_panneau, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
				.addPreferredGap(LayoutStyle.ComponentPlacement.RELATED, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
				.addComponent(imprimerTableDeVerite_bouton)
				.addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
		);

		// Fen�tre :
		this.setIconImage(Toolkit.getDefaultToolkit().getImage("image/La Logicienne.gif"));
		setResizable(false);
		
		GroupLayout layout = new GroupLayout(getContentPane());
		getContentPane().setLayout(layout);
		layout.setHorizontalGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
			.addComponent(global_jPanel, GroupLayout.Alignment.TRAILING, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, global_jPanel.getWidth())
		);
		layout.setVerticalGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
			.addComponent(global_jPanel, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, global_jPanel.getHeight())
		);
		pack();
		setLocationRelativeTo(null);
	}

/************
 * M�thodes :
 ************/
	public void valueChanged(ListSelectionEvent event) {
		if(event.getSource() == numerotationLignes_table.getSelectionModel() && event.getValueIsAdjusting())
			tablesDeVeriteAtomes_jTable.setRowSelectionInterval(numerotationLignes_table.getSelectedRow(), numerotationLignes_table.getSelectedRow());
	}

	public boolean isCellEditable(int rowIndex, int columnIndex) {
		return false;
	}
}