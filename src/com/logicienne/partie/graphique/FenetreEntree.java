package com.logicienne.partie.graphique;
/*
 * Nom de la classe : FenetreEntree
 *
 * Description		: Il est question de la fen�tre de d�marrage ; cette classe contient, en effet, la m�thode "main".
 * 					  L'utilisateur entre une expression logique. (Pour ce faire, il peut aussi employer le clavier physique.)
 * 					  Puis il clique sur le bouton "Calcu." (ou tape sur la touche "Entr�e", voire la touche "Entr") :
 * 					  si une EBF a �t� saisie, l'application retourne l'ensemble des valeurs de v�rit� de l'expression ;
 * 					  sinon, elle retourne un message d'erreur.
 * 					  Dans le cas o� le "r�sultat" est correct, il est possible de faire afficher la table de v�rit� ;
 * 					  sinon appara�t une bo�te de dialogue pr�cisant que "l'obtention de la table de v�rit� est [pour
 * 					  le coup] impossible"...
 *
 * Version			: 1.0
 *
 * Cr�ation			: du 21/09/2015 au 21/10/2015
 *
 * Copyright		: Cyril Marilier
 */
import java.awt.Color;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.Toolkit;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.BorderFactory;
import javax.swing.GroupLayout;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.LayoutStyle;
import javax.swing.ToolTipManager;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.WindowConstants;

import javax.swing.border.LineBorder;

import com.logicienne.entites.Atome;
import com.logicienne.entites.Connecteur;
import com.logicienne.entites.Proposition;

import com.logicienne.enumerations.Atom;
import com.logicienne.enumerations.Operator;

import com.logicienne.partie.intelligente.Applicatif;

public class FenetreEntree extends JFrame
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String expression, copieExpression;
	private boolean echec;

/*****************************************
 * D�claration des variables de contr�le :
 *****************************************/
	// Champ d'affichage de l'entr�e :
	private ChampTexte saisie_champTexte;

	// Pav� de saisie (de l'expression) :
	private Bouton p_bouton;
	private Bouton q_bouton;
	private Bouton r_bouton;
	private Bouton s_bouton;
	private Bouton t_bouton;

	private Bouton conjonction_bouton;
	private Bouton disjonctionInclusive_bouton;
	private Bouton negation_bouton;
	private Bouton parentheseOuvrante_bouton;
	private Bouton parentheseFermante_bouton;

	private Bouton effacerSelection_bouton;
	private Bouton toutEffacer_bouton;

	private Bouton calculer_bouton;

	private JPanel saisir_pave;

	// Champ d'affichage de la sortie :
	private ChampTexte resultat_champTexte;

	// Pav� d'affichage de la table de v�rit� (de l'expression) :
	private Bouton afficherTable_bouton;

	private JPanel afficherTable_pave;

	// Panneau global de la fen�tre :
	private JPanel global_jPanel;

/********************************
 * Constructeur de la "fen�tre" :
 ********************************/
	public FenetreEntree() {
		// Champ d'affichage de l'entr�e :
		String description = "&nbsp;&nbsp;<font style='font-style:normal'>�CRAN DE SAISIE :</font> Respectez les r�gles de formation !<br/>"
						   + "<br/>"
						   + "&nbsp;&nbsp;Un atome �tant une EBF (Expression Bien Form�e),<br/>"
						   + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>si A est une EBF alors <font color='#FF7F00'>�A</font> est une EBF</b>,<br/>"
						   + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>si A et B sont des EBF alors <font color='#FF7F00'>(A&B)</font> est une EBF</b>,<br/>"
						   + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>si A et B sont des EBF alors <font color='#FF7F00'>(A+B)</font> est une EBF</b>...<br/>"
						   + "&nbsp;&nbsp;(Toute expression non form�e suivant ces r�gles est mal<br/>"
						   + "&nbsp;&nbsp;form�e.*)<br/>"
						   + "<br/>"
						   + "&nbsp;&nbsp;Par exemple, � (�(p+��s)&((�r+q)&�(�r+q))) � est une&nbsp;&nbsp;<br/>"
						   + "&nbsp;&nbsp;EBF.<br/>"
						   + "&nbsp;&nbsp;_______<br/>"
						   + "&nbsp;&nbsp;<font style='font-size:10px'>* On autorisera toutefois d�omettre les parenth�ses ext�rieures :</font><br/>"
						   + "&nbsp;&nbsp;<font style='font-size:10px'>au lieu de � (�(p+��s)&((�r+q)&�(�r+q))) �, on pourra �crire,</font><br/>"
						   + "&nbsp;&nbsp;<font style='font-size:10px'>par cons�quent, � �(p+��s)&((�r+q)&�(�r+q)) �.</font>";
		saisie_champTexte = new ChampTexte(Color.BLACK, new Color(154, 188, 255), description);
		saisie_champTexte.setHorizontalAlignment(JLabel.CENTER);
		ToolTipManager.sharedInstance().setInitialDelay(147);
		ToolTipManager.sharedInstance().setDismissDelay(299792);
		saisie_champTexte.addKeyListener(new KeyAdapter() {
			public void keyTyped(KeyEvent ke) {
				if (String.valueOf(ke.getKeyChar()).matches("[p-t]|\\(|\\)|&|\\+|\\x08"))
					saisir(null);
				else {
					ke.consume();
					if (ke.getKeyChar() == '-')
						saisir("�");
					else
						if (ke.getKeyChar() == '\n')
							calculer();
						else
							if (ke.getKeyChar() == '\u007F') {
								resultat_champTexte.setText(null);
								saisie_champTexte.setText(null);
								saisie_champTexte.requestFocus();
							}
				}
			}
		});

		// Pav� de saisie (de l'expression) :
		p_bouton = new Bouton(Color.WHITE, new Color(123, 160, 91), Atom.P.getLettre(), "&nbsp;&nbsp;atome � <font color='#FF7F00'>p</font> �&nbsp;&nbsp;");
		p_bouton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent ae) {
				saisir("p");
			}
		});
		q_bouton = new Bouton(Color.WHITE, new Color(123, 160, 91), Atom.Q.getLettre(), "&nbsp;&nbsp;atome � <font color='#FF7F00'>q</font> �&nbsp;&nbsp;");
		q_bouton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent ae) {
				saisir("q");
			}
		});
		r_bouton = new Bouton(Color.WHITE, new Color(123, 160, 91), Atom.R.getLettre(), "&nbsp;&nbsp;atome � <font color='#FF7F00'>r</font> �&nbsp;&nbsp;");
		r_bouton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent ae) {
				saisir("r");
			}
		});
		s_bouton = new Bouton(Color.WHITE, new Color(123, 160, 91), Atom.S.getLettre(), "&nbsp;&nbsp;atome � <font color='#FF7F00'>s</font> �&nbsp;&nbsp;");
		s_bouton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent ae) {
				saisir("s");
			}
		});
		t_bouton = new Bouton(Color.WHITE, new Color(123, 160, 91), Atom.T.getLettre(), "&nbsp;&nbsp;atome � <font color='#FF7F00'>t</font> �&nbsp;&nbsp;");
		t_bouton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent ae) {
				saisir("t");
			}
		});

		conjonction_bouton = new Bouton(Color.WHITE, new Color(91, 123, 160), Operator.CONJONCTION.getSigne(), "&nbsp;&nbsp;conjonction (� <font color='#FF7F00'>ET</font> �)&nbsp;&nbsp;");
		conjonction_bouton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent ae) {
				saisir("&");
			}
		});
		disjonctionInclusive_bouton = new Bouton(Color.WHITE, new Color(91, 123, 160), Operator.DISJONCTION_INCLUSIVE.getSigne(), "&nbsp;&nbsp;disjonction inclusive (� <font color='#FF7F00'>OU</font> [<font color='#FF7F00'>inclusif</font>] �)&nbsp;&nbsp;");
		disjonctionInclusive_bouton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent ae) {
				saisir("+");
			}
		});
		negation_bouton = new Bouton(Color.WHITE, new Color(91, 123, 160), Operator.NEGATION.getSigne(), "&nbsp;&nbsp;n�gation (� <font color='#FF7F00'>NON</font> �)&nbsp;&nbsp;");
		negation_bouton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent ae) {
				saisir("�");
			}
		});
		parentheseOuvrante_bouton = new Bouton(Color.WHITE, new Color(45, 64, 1), "(", "&nbsp;&nbsp;� <font color='#FF7F00'>parenth�se ouvrante</font> �&nbsp;&nbsp;");
		parentheseOuvrante_bouton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent ae) {
				saisir("(");
			}
		});
		parentheseFermante_bouton = new Bouton(Color.WHITE, new Color(45, 64, 1), ")", "&nbsp;&nbsp;� <font color='#FF7F00'>parenth�se fermante</font> �&nbsp;&nbsp;");
		parentheseFermante_bouton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent ae) {
				saisir(")");
			}
		});

		effacerSelection_bouton = new Bouton(Color.WHITE, new Color(255, 94, 77), "ES", "&nbsp;&nbsp;Effacer la s�lection !&nbsp;&nbsp;");
		effacerSelection_bouton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent ae) {
				if (!saisie_champTexte.getText().isEmpty()) {
					resultat_champTexte.setText(null);
					if (saisie_champTexte.getSelectedText() != null)
						saisie_champTexte.setText(new StringBuffer(saisie_champTexte.getText()).delete(saisie_champTexte.getSelectionStart(), saisie_champTexte.getSelectionEnd()).toString());
					else {
						int positionCurseur = saisie_champTexte.getCaretPosition();
						saisie_champTexte.setText(new StringBuffer(saisie_champTexte.getText()).deleteCharAt(positionCurseur - 1).toString());
						saisie_champTexte.setCaretPosition(positionCurseur - 1);
					}
				}
				saisie_champTexte.requestFocus();
			}
		});
		toutEffacer_bouton = new Bouton(new Color(255, 94, 77), Color.WHITE, "TE", "&nbsp;&nbsp;Tout effacer !&nbsp;&nbsp;");
		toutEffacer_bouton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent ae) {
				resultat_champTexte.setText(null);
				saisie_champTexte.setText(null);
				saisie_champTexte.requestFocus();
			}
		});

		calculer_bouton = new Bouton(new Color(188, 154, 255), Color.WHITE, "Calcul.", "&nbsp;&nbsp;Calculer les valeurs de v�rit� de l�expression.&nbsp;&nbsp;");
		calculer_bouton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent ae) {
				calculer();
			}
		});

		saisir_pave = new JPanel();
		saisir_pave.setBorder(BorderFactory.createLineBorder(new Color(1, 45, 64), 2));
		saisir_pave.setBackground(new Color(91, 123, 160));

		GroupLayout saisie_layout = new GroupLayout(saisir_pave);
		saisir_pave.setLayout(saisie_layout);
		saisie_layout.setHorizontalGroup(saisie_layout.createParallelGroup(GroupLayout.Alignment.LEADING)
			.addGroup(saisie_layout.createSequentialGroup()
				.addContainerGap()
				.addGroup(saisie_layout.createParallelGroup(GroupLayout.Alignment.LEADING)
					.addGroup(saisie_layout.createSequentialGroup()
						.addComponent(p_bouton, GroupLayout.PREFERRED_SIZE, 64, GroupLayout.PREFERRED_SIZE)
						.addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
						.addComponent(q_bouton, GroupLayout.PREFERRED_SIZE, 64, GroupLayout.PREFERRED_SIZE)
						.addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
						.addComponent(r_bouton, GroupLayout.PREFERRED_SIZE, 64, GroupLayout.PREFERRED_SIZE)
						.addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
						.addComponent(s_bouton, GroupLayout.PREFERRED_SIZE, 64, GroupLayout.PREFERRED_SIZE)
						.addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
						.addComponent(t_bouton, GroupLayout.PREFERRED_SIZE, 64, GroupLayout.PREFERRED_SIZE))
					.addGroup(saisie_layout.createSequentialGroup()
						.addComponent(conjonction_bouton, GroupLayout.PREFERRED_SIZE, 64, GroupLayout.PREFERRED_SIZE)
						.addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
						.addComponent(disjonctionInclusive_bouton, GroupLayout.PREFERRED_SIZE, 64, GroupLayout.PREFERRED_SIZE)
						.addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
						.addComponent(negation_bouton, GroupLayout.PREFERRED_SIZE, 64, GroupLayout.PREFERRED_SIZE)
						.addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
						.addComponent(parentheseOuvrante_bouton, GroupLayout.PREFERRED_SIZE, 64, GroupLayout.PREFERRED_SIZE)
						.addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
						.addComponent(parentheseFermante_bouton, GroupLayout.PREFERRED_SIZE, 64, GroupLayout.PREFERRED_SIZE)))
					.addGap(18, 18, 18)
					.addGroup(saisie_layout.createParallelGroup(GroupLayout.Alignment.LEADING, false)
						.addGroup(saisie_layout.createSequentialGroup()
							.addComponent(effacerSelection_bouton, GroupLayout.PREFERRED_SIZE, 64, GroupLayout.PREFERRED_SIZE)
							.addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
							.addComponent(toutEffacer_bouton, GroupLayout.PREFERRED_SIZE, 64, GroupLayout.PREFERRED_SIZE))
						.addComponent(calculer_bouton, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
					.addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
		);
		saisie_layout.setVerticalGroup(saisie_layout.createParallelGroup(GroupLayout.Alignment.LEADING)
			.addGroup(saisie_layout.createSequentialGroup()
				.addGap(18, 18, 18)
				.addGroup(saisie_layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
					.addComponent(p_bouton, GroupLayout.PREFERRED_SIZE, 64, GroupLayout.PREFERRED_SIZE)
					.addComponent(q_bouton, GroupLayout.PREFERRED_SIZE, 64, GroupLayout.PREFERRED_SIZE)
					.addComponent(r_bouton, GroupLayout.PREFERRED_SIZE, 64, GroupLayout.PREFERRED_SIZE)
					.addComponent(s_bouton, GroupLayout.PREFERRED_SIZE, 64, GroupLayout.PREFERRED_SIZE)
					.addComponent(effacerSelection_bouton, GroupLayout.PREFERRED_SIZE, 64, GroupLayout.PREFERRED_SIZE)
					.addComponent(toutEffacer_bouton, GroupLayout.PREFERRED_SIZE, 64, GroupLayout.PREFERRED_SIZE)
					.addComponent(t_bouton, GroupLayout.PREFERRED_SIZE, 64, GroupLayout.PREFERRED_SIZE))
				.addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
				.addGroup(saisie_layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
					.addComponent(parentheseOuvrante_bouton, GroupLayout.PREFERRED_SIZE, 64, GroupLayout.PREFERRED_SIZE)
					.addComponent(parentheseFermante_bouton, GroupLayout.PREFERRED_SIZE, 64, GroupLayout.PREFERRED_SIZE)
					.addComponent(calculer_bouton, GroupLayout.PREFERRED_SIZE, 64, GroupLayout.PREFERRED_SIZE)
					.addComponent(disjonctionInclusive_bouton, GroupLayout.PREFERRED_SIZE, 64, GroupLayout.PREFERRED_SIZE)
					.addComponent(negation_bouton, GroupLayout.PREFERRED_SIZE, 64, GroupLayout.PREFERRED_SIZE)
					.addComponent(conjonction_bouton, GroupLayout.PREFERRED_SIZE, 64, GroupLayout.PREFERRED_SIZE))
				.addContainerGap(16, Short.MAX_VALUE))
		);

		// Champ d'affichage de la sortie :
		resultat_champTexte = new ChampTexte(new Color(154, 188, 255), Color.BLACK, null);
		resultat_champTexte.setBorder(new LineBorder(Color.BLACK, 2));
		resultat_champTexte.setFont(new Font("Courier New", 1, 25));
		resultat_champTexte.setText(" �CRAN DE R�SULTAT");
		resultat_champTexte.setEditable(false);

		// Pav� d'affichage de la table de v�rit� (de l'expression) :
		afficherTable_bouton = new Bouton(Color.WHITE, new Color(1, 45, 64), "Afficher la table de v�rit�...", null);
		afficherTable_bouton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent ae) {
				if (echec && !resultat_champTexte.getText().isEmpty()) {
					FenetreSortie fenetreTableDeVerite = new FenetreSortie(copieExpression);
					fenetreTableDeVerite.setVisible(true);
				}
				else
					JOptionPane.showMessageDialog(null, "L'obtention de la table de v�rit� est impossible...", "Affichage impossible !", JOptionPane.ERROR_MESSAGE);
				saisie_champTexte.requestFocus();
			}
		});

		afficherTable_pave = new JPanel();
		afficherTable_pave.setBorder(BorderFactory.createLineBorder(new Color(1, 45, 64), 2));
		afficherTable_pave.setBackground(new Color(91, 123, 160));

		GroupLayout afficherTable_layout = new GroupLayout(afficherTable_pave);
		afficherTable_pave.setLayout(afficherTable_layout);
		afficherTable_layout.setHorizontalGroup(afficherTable_layout.createParallelGroup(GroupLayout.Alignment.LEADING)
			.addGroup(GroupLayout.Alignment.TRAILING, afficherTable_layout.createSequentialGroup()
				.addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
				.addComponent(afficherTable_bouton, GroupLayout.PREFERRED_SIZE, 498, GroupLayout.PREFERRED_SIZE)
				.addContainerGap())
		);
		afficherTable_layout.setVerticalGroup(afficherTable_layout.createParallelGroup(GroupLayout.Alignment.LEADING)
			.addGroup(afficherTable_layout.createSequentialGroup()
				.addContainerGap()
				.addComponent(afficherTable_bouton, GroupLayout.PREFERRED_SIZE, 62, GroupLayout.PREFERRED_SIZE)
				.addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
		);

		// Panneau (global) de la fen�tre :
		global_jPanel = new JPanel();
		global_jPanel.setBackground(Color.WHITE);
		global_jPanel.setPreferredSize(new Dimension(532, 424));

		GroupLayout global_layout = new GroupLayout(global_jPanel);
		global_jPanel.setLayout(global_layout);
		global_layout.setHorizontalGroup(global_layout.createParallelGroup(GroupLayout.Alignment.LEADING)
			.addGroup(global_layout.createSequentialGroup()
				.addContainerGap()
				.addGroup(global_layout.createParallelGroup(GroupLayout.Alignment.LEADING)
					.addGroup(global_layout.createParallelGroup(GroupLayout.Alignment.LEADING, false)
						.addComponent(saisir_pave, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
						.addComponent(resultat_champTexte)
						.addComponent(saisie_champTexte))
					.addComponent(afficherTable_pave, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
				.addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
		);
		global_layout.setVerticalGroup(global_layout.createParallelGroup(GroupLayout.Alignment.LEADING)
			.addGroup(global_layout.createSequentialGroup()
				.addContainerGap()
				.addComponent(saisie_champTexte, GroupLayout.PREFERRED_SIZE, 48, GroupLayout.PREFERRED_SIZE)
				.addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
				.addComponent(saisir_pave, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
				.addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
				.addComponent(resultat_champTexte, GroupLayout.PREFERRED_SIZE, 48, GroupLayout.PREFERRED_SIZE)
				.addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
				.addComponent(afficherTable_pave, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
				.addContainerGap(31, Short.MAX_VALUE))
		);

		// Fen�tre :
		this.setIconImage(Toolkit.getDefaultToolkit().getImage("image/La Logicienne.gif"));
		setTitle("La Logicienne � Cyril Marilier, octobre 2015 (version 1.0)");
		setPreferredSize(new Dimension(532, 424));
		setResizable(false);
		setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

		GroupLayout layout = new GroupLayout(getContentPane());
		getContentPane().setLayout(layout);
		layout.setHorizontalGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
			.addComponent(global_jPanel, GroupLayout.DEFAULT_SIZE, 542, Short.MAX_VALUE)
		);
		layout.setVerticalGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
			.addGroup(layout.createSequentialGroup()
				.addComponent(global_jPanel, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
				.addContainerGap())
		);
		pack();
		setLocationRelativeTo(null);
	}

/************
 * M�thodes :
 ************/
	// M�thode "saisir" :
	private void saisir(String signe) {
		resultat_champTexte.setText(null);
		saisie_champTexte.replaceSelection(signe);
		saisie_champTexte.requestFocus();
	}

	// M�thode "calculer" :
	public void calculer() {
		Atome.sExplicites().removeAllElements();
		Proposition.sEnJeu().removeAllElements();
		Connecteur.sEnJeu.removeAllElements();
		echec = false;
		if (saisie_champTexte.getText().length() < 2)
			resultat_champTexte.setText(Applicatif.afficher_messageErreur(saisie_champTexte.getText()));
		else {
			expression = saisie_champTexte.getText();
			copieExpression = expression;
			// "nCB" pour "nombre de Connecteurs Binaires",
			// "nPO" pour "nombre de Parentheses Ouvrantes",
			// "nPF" pour "nombre de Parentheses Fermantes"...
			int nCB = 0, nPO = 0, nPF = 0;
			for (int i = 0; i < expression.length(); i++)
				switch (expression.charAt(i)) {
					case '&':
					case '+': nCB++;
							  break;
					case '(': nPO++;
							  break;
					case ')': nPF++;
				}
			if (nPF == nPO && nCB == nPF + 1)
				expression = "(" + expression + ")";
			echec = Applicatif.traiter(expression);
			if (echec) {
				String tableDeVerite = "";
				for (boolean valeurDeVerite : Proposition.sEnJeu().lastElement().getTableDeVerite())
					tableDeVerite += valeurDeVerite ? 1 : 0;
				if (!tableDeVerite.contains("0"))
					resultat_champTexte.setText(" L'expression est une tautologie.");
				else
					if (!tableDeVerite.contains("1"))
						resultat_champTexte.setText(" L'expression est une antilogie.");
					else
						resultat_champTexte.setText(" " + tableDeVerite);
			}
			else
				resultat_champTexte.setText(" L'expression est mal form�e...");
		}
		saisie_champTexte.requestFocus();
	}

	// M�thode "main" :
	public static void main(String[] args) {
		try {
			for (UIManager.LookAndFeelInfo info : UIManager.getInstalledLookAndFeels())
				if ("Nimbus".equals(info.getName())) {
					UIManager.setLookAndFeel(info.getClassName());
					UIManager.put("info", Color.BLACK);
					break;
				}
		} catch (ClassNotFoundException cnfe) {
			Logger.getLogger(FenetreEntree.class.getName()).log(Level.SEVERE, null, cnfe);
		} catch (InstantiationException ie) {
			Logger.getLogger(FenetreEntree.class.getName()).log(Level.SEVERE, null, ie);
		} catch (IllegalAccessException iae) {
			Logger.getLogger(FenetreEntree.class.getName()).log(Level.SEVERE, null, iae);
		} catch (UnsupportedLookAndFeelException ulafe) {
			Logger.getLogger(FenetreEntree.class.getName()).log(Level.SEVERE, null, ulafe);
		}
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				new FenetreEntree().setVisible(true);
			}
		});
	}
}