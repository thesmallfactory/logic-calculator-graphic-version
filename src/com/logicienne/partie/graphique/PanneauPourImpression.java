package com.logicienne.partie.graphique;
/*
 * Nom de la classe : PanneauPourImpression
 *
 * Description		: Gr�ce � cette classe, l'auteur peut imprimer le contenu du panneau de la table de v�rit�.
 *
 * Version			: 1.0
 *
 * Cr�ation			: du 21/09/2015 au 21/10/2015
 *
 * Copyright		: Cyril Marilier
 */
import java.awt.Graphics;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import java.awt.print.PageFormat;
import java.awt.print.Printable;
import java.awt.print.PrinterException;
import java.awt.print.PrinterJob;

import javax.swing.JPanel;

public class PanneauPourImpression extends JPanel implements Printable, ActionListener
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private JPanel panneauTable;

	public int print(Graphics imageContenu, PageFormat formatPage, int nombrePages) throws PrinterException {
		if (nombrePages > 0)
			return NO_SUCH_PAGE;
		imageContenu.translate(64, 72);
		panneauTable.printAll(imageContenu);
		return PAGE_EXISTS;
	}

	public void actionPerformed(ActionEvent e) {
		PrinterJob travailImprimante = PrinterJob.getPrinterJob();
		PageFormat formatPage = travailImprimante.defaultPage();
		formatPage.setOrientation(PageFormat.LANDSCAPE);
		travailImprimante.setPrintable(this, formatPage);
		if (travailImprimante.printDialog())
			try {
				travailImprimante.print();
			} catch (PrinterException pe) {
				System.out.println("Impression impossible !");
			}
	}

	public PanneauPourImpression(JPanel panneauTable) {
		this.panneauTable = panneauTable;
	}
}