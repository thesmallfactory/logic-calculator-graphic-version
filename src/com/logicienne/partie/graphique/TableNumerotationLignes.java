package com.logicienne.partie.graphique;
/*
 * Nom de la classe : TableNumerotationLignes
 *
 * Description		: Gr�ce � cette classe, l'auteur interdit � l'utilisateur d'�crire dans les cellules
 * 					  de la "table" des num�ros des lignes...
 *
 * Version			: 1.0
 *
 * Cr�ation			: du 21/09/2015 au 21/10/2015
 *
 * Copyright		: Cyril Marilier
 */
import javax.swing.JTable;

public class TableNumerotationLignes extends JTable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public TableNumerotationLignes(String[][] NumerotationDesLignes, String[] NumeroPremiereLigne) {
		super(NumerotationDesLignes, NumeroPremiereLigne);
	}

	public boolean isCellEditable(int ligne, int colonne) {
		return false;
	}
}