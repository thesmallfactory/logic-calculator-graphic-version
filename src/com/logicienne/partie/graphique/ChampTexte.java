package com.logicienne.partie.graphique;
/*
 * Nom de la classe : ChampTexte
 *
 * Description		: Gr�ce � cette classe, l'auteur associe au champ d'affichage de l'entr�e une
 * 					  infobulle "originale"...
 *
 * Version			: 1.0
 *
 * Cr�ation			: du 21/09/2015 au 21/10/2015
 *
 * Copyright		: Cyril Marilier
 */
import java.awt.Color;
import java.awt.Font;

import javax.swing.BorderFactory;
import javax.swing.JTextField;
import javax.swing.JToolTip;

public class ChampTexte extends JTextField
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public ChampTexte(Color couleurArrierePlan, Color couleurPremierPlan, String description) {
		this.setBackground(couleurArrierePlan);
		this.setForeground(couleurPremierPlan);
		this.setFont(new Font("Courier New", 1, 25));
		if (description != null) {
			this.createToolTip();
			this.setToolTipText("<html>"
								  + "<br/>"
								  + "<font style='font-family:Times New Roman;font-size:12px;font-style:italic;color:WHITE'>"
								  	  + description
								  + "</font><br/>"
								  + "<br/>"
							  + "</html>");
		}
	}

	public JToolTip createToolTip() {
		JToolTip infoBulle = super.createToolTip();
		infoBulle.setBorder(BorderFactory.createLineBorder(new Color(255, 127, 0), 2));
		return infoBulle;
	}
}