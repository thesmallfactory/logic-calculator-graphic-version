package com.logicienne.partie.graphique;
/*
 * Nom de la classe : TableAtomes
 *
 * Description		: Gr�ce � cette classe, l'auteur associe � la table de v�rit� des atomes une info-
 * 					  bulle "originale"...
 *
 * Version			: 1.0
 *
 * Cr�ation			: du 21/09/2015 au 21/10/2015
 *
 * Copyright		: Cyril Marilier
 */
import java.awt.Color;

import javax.swing.BorderFactory;
import javax.swing.JTable;
import javax.swing.JToolTip;

public class TableAtomes extends JTable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public TableAtomes(String[][] tablesDeVeriteAtomes, String[] nomsAtomes) {
		super(tablesDeVeriteAtomes, nomsAtomes);
		this.createToolTip();
	}

	public JToolTip createToolTip() {
		JToolTip infoBulle = super.createToolTip();
		infoBulle.setBorder(BorderFactory.createLineBorder(new Color(255, 127, 0), 2));
		infoBulle.setBackground(Color.BLACK);
		return infoBulle;
	}
}