package com.logicienne.partie.graphique;
/*
 * Nom de la classe : Bouton
 *
 * Description		: Par cette classe, l'auteur, en plus d'associer � chaque bouton une infobulle
 * 					  "originale", �conomise un certain nombre de lignes de code...
 *
 * Version			: 1.0
 *
 * Cr�ation			: du 21/09/2015 au 21/10/2015
 *
 * Copyright		: Cyril Marilier
 */
import java.awt.Color;
import java.awt.Font;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JToolTip;

import javax.swing.border.LineBorder;

public class Bouton extends JButton
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public Bouton(Color couleurArrierePlan, Color couleurPremierPlan, String signe, String description) {
		this.setOpaque(true);
		this.setBorder(new LineBorder(new Color(1, 45, 64), 1));
		this.setBackground(couleurArrierePlan);
		this.setForeground(couleurPremierPlan);
		this.setFont(new Font("Courier New", 1, 25));
		this.setText(signe);
		if (description != null) {
			this.createToolTip();
			this.setToolTipText("<html>"
								  + "<font style='font-family:Times New Roman;font-size:12px;font-style:italic;color:WHITE'>"
								  	  + description
								  + "</font><br/>"
								  + "<font style='font-size:1px'>"
								  	  + "&nbsp;"
								  + "</font>"
							  + "</html>");
		}
	}

	public JToolTip createToolTip() {
		JToolTip infoBulle = super.createToolTip();
		infoBulle.setBorder(BorderFactory.createLineBorder(new Color(255, 127, 0), 2));
		return infoBulle;
	}
}