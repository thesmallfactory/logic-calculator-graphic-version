package com.logicienne.partie.intelligente;
/*
 * Nom de la classe : Applicatif
 *
 * Description		: Le code de cette classe correspond à la partie lourde de l'application...
 *
 * Version			: 1.0
 *
 * Création			: du 21/09/2015 au 21/10/2015
 *
 * Copyright		: Cyril Marilier
 */
import java.util.Vector;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.logicienne.entites.Atome;
import com.logicienne.entites.Connecteur;
import com.logicienne.entites.ConnecteurBinaire;
import com.logicienne.entites.ConnecteurUnaire;
import com.logicienne.entites.Proposition;

import com.logicienne.enumerations.Atom;
import com.logicienne.enumerations.Operator;

public class Applicatif
{
	private static Vector<String> niveau;
	private static Vector<Vector<String>> table;

/*********************
 * MÉTHODE "TRAITER" :
 *********************/
	public static boolean traiter(String expression) {
		boolean effectivite = false;
		// PREMIÈRE ANALYSE : "L'expression, présente-t-elle au moins un connecteur et un atome ?"
		if (premiereAnalyse(expression)) {
			// A (instanciation des atomes de l'expression) :
			instancier_Atomes(expression);
			// B (compilation de l'expression de sorte que le programme puisse exercer un "traitement",
			// notamment "déterminer si l'expression est une EBF") :
			expression = compiler(expression);
			// SECONDE ANALYSE : "L'expression, est-elle une EBF ?"
			// (En fait, si la décomposition aboutit, l'expression est "bien formée".)
			if (secondeAnalyse(expression)) {
				// C (calcul de "la table de vérité"...) :
				dernierTraitement();
				effectivite = true;
			}
		}
		return effectivite;
	}

/*****************************************************************************************
 * PREMIÈRE ANALYSE : "L'expression, présente-t-elle au moins un connecteur et un atome ?"
 *****************************************************************************************/
	public static boolean premiereAnalyse(String expression) {
		/*"L'expression, présente-t-elle au moins un connecteur ?"*/
		boolean valeur = diagnostic_connecteurs(expression);
		/*"Si oui, présente-t-elle au moins un atome ?"*/
		if (valeur)
			diagnostic_atomes(expression);
		return valeur;
	}

	// diagnostic_connecteurs :
	public static boolean diagnostic_connecteurs(String expression) {
		boolean existence = true;
		if (expression.contains("¬")) new ConnecteurUnaire(Operator.NEGATION.getSigne());
		if (expression.contains("&")) new ConnecteurBinaire(Operator.CONJONCTION.getSigne());
		if (expression.contains("+")) new ConnecteurBinaire(Operator.DISJONCTION_INCLUSIVE.getSigne());
		if (Connecteur.sEnJeu.isEmpty())
			existence = false;
		return existence;
	}

	// diagnostic_atomes :
	public static void diagnostic_atomes(String expression) {
		for (Atom atome : Atom.values())
			if (expression.contains(atome.getLettre()))
				new Atome("p[0." + (atome.ordinal() + 1) + "]");
	}

	/************************************************
	 * A (instanciation des atomes de l'expression) :
	 ************************************************/
	public static void instancier_Atomes(String expression) {
		for (byte b = 0; b < Atome.sExplicites().size(); b++)
			Atome.sExplicites().elementAt(b).instantiated(nombreLignes_tableDeVerite(nombreDeMasse()), numeroAtomique(b));
	}

	// nombreLignes_tableDeVerite :
	public static byte nombreLignes_tableDeVerite(byte nombreAtomes) {
		byte nombreLignes_tableDeVerite = 0;
		if (nombreAtomes > 0)
			nombreLignes_tableDeVerite = (byte)Math.pow(2, nombreAtomes);
		return nombreLignes_tableDeVerite;
	}

	// nombreDeMasse (= nombre d'atomes explicites + nombre d'atomes implicites) :
	public static byte nombreDeMasse() {
		byte b_lastElement = (byte)Atome.sExplicites().indexOf(Atome.sExplicites().lastElement());
		return numeroAtomique(b_lastElement);
	}

	// numeroAtomique :
	public static byte numeroAtomique(byte b_element) {
		int preIndice = Atome.sExplicites().elementAt(b_element).getNom().indexOf(".");
		int postIndice = Atome.sExplicites().elementAt(b_element).getNom().indexOf("]");
		return Byte.parseByte(Atome.sExplicites().elementAt(b_element).getNom().substring(preIndice + 1, postIndice));
	}

	/**************************************
	 * B (compilation de l'expression...) :
	 **************************************/
	public static String compiler(String expression) {
		byte i = 0;
		for (Atom atome : Atom.values())
			if (expression.contains(atome.getLettre()))
				expression = expression.replace(atome.getLettre(), Atome.sExplicites().elementAt(i++).getNom());
		return expression;
	}

/*******************************************************************************
 * SECONDE ANALYSE : "L'expression, est-elle une EBF (Expression Bien Formée) ?"
 *******************************************************************************/
	public static boolean secondeAnalyse(String expression) {
		table = new Vector<>();
		float situation = 0.0F;
		do {
			situation += 1.0F;
			expression = decomposer(expression, situation);
		} while (!niveau.isEmpty());
		boolean EBFs = expression.matches("p\\[\\d+\\.\\d+\\]");
		return EBFs;
	}

	// decomposer :
	public static String decomposer(String expression, float situation) {
		String saisie = "";
		String regex = "";
		niveau = new Vector<>();
		for (Connecteur connecteur : Connecteur.sEnJeu)
			 regex += connecteur + (connecteur != Connecteur.sEnJeu.lastElement() ? "|" : "");
/*
		regex = Connecteur.sEnJeu.stream().map((connecteur) -> connecteur + (connecteur != Connecteur.sEnJeu.lastElement() ? "|" : "")).reduce(regex, String::concat);
*/
		Pattern motif = Pattern.compile(regex);
		Matcher correspondant = motif.matcher(expression);
		int i = 0;
		while (correspondant.find()) {
			for (; i < correspondant.start(); i++)
				saisie += expression.charAt(i);
			niveau.add(correspondant.group());
			situation = Math.round((situation + 0.1)*10)/10F;
			saisie += "p[" + situation + "]";
			i += correspondant.group().length();
		}
		table.add(niveau);
		if (i < expression.length())
			for (; i < expression.length(); i++)
				saisie += expression.charAt(i);
		return saisie;
	}

	/*****************************************
	 * C (calcul de "la table de vérité"...) :
	 *****************************************/
	public static void dernierTraitement() {
		for (int ligne = 0; ligne < table.size(); ligne++)
			for (int colonne = 0; colonne < table.elementAt(ligne).size(); colonne++) {
				String expression = table.elementAt(ligne).elementAt(colonne);
				if (Connecteur.sEnJeu.elementAt(0) != null)
					for (byte b = 0; b < Connecteur.sEnJeu.size(); b++) {
						if (Connecteur.sEnJeu.elementAt(b).getSigne().matches("^[" + Operator.NEGATION.getSigne() + "]$"))
							ConnecteurUnaire.sEnJeu.elementAt(b).interpreted(nombreLignes_tableDeVerite(nombreDeMasse()), expression, ligne, colonne);
							if (Connecteur.sEnJeu.elementAt(b).getSigne().matches("^[" + Operator.CONJONCTION.getSigne() + Operator.DISJONCTION_INCLUSIVE.getSigne() + "]$"))
								ConnecteurBinaire.sEnJeu.elementAt(b).interpreted(nombreLignes_tableDeVerite(nombreDeMasse()), expression, ligne, colonne);
						}
			}
	}

/**********************************************************
 * MÉTHODE pour afficher un message d'erreur en résultat...
 **********************************************************/
	public static String afficher_messageErreur(String expression) {
		String messageErreur = "";
		switch (expression.length()) {
			case 0 : messageErreur = " Saisissez une expression !";
					 break;
			case 1 : messageErreur = " L'expression est " + (expression.matches("^[pqrst]$") ? "un atome..." : "mal formée.");
		}
		return messageErreur;
	}

/*******************
 * AUTRES MÉTHODES :
 *******************/
	// tablesDeVerite_atomes (constitution des tables de vérité des atomes (de l'expression)) :
	public static String[][] tablesDeVerite_atomes(String expression) {
		String[][] valeursDeVerite = new String[nombreLignes_tableDeVerite(nombreDeMasse())][Atome.sExplicites().size()];
		for (byte ligne = 0; ligne < nombreLignes_tableDeVerite(nombreDeMasse()); ligne++)
			for (byte colonne = 0; colonne < Atome.sExplicites().size(); colonne++)
				valeursDeVerite[ligne][colonne] = convertir(Atome.sExplicites().elementAt(colonne).getTableDeVerite()[ligne]);
		return valeursDeVerite;
	}

	// tableDeVerite (constitution de la table de vérité (de l'expression)) :
	public static String[][] tableDeVerite(String expression) {
		Vector<Integer> ordreColonnesDeVerite = calculer_ordreColonnesDeVerite();
		expression = expression.replaceAll("[" + Atom.values()[0].getLettre() + "-" + Atom.values()[Atom.values().length - 1].getLettre() + "()]+", " ");
		String[][] valeursDeVerite = new String[nombreLignes_tableDeVerite(nombreDeMasse()) + 1][expression.length()];
		for (byte ligne = 0; ligne < nombreLignes_tableDeVerite(nombreDeMasse()); ligne++) {
			int m = 0;
			for (int colonne = 0; colonne < expression.length(); colonne++) {
				String caractere = Character.toString(expression.charAt(colonne));
				valeursDeVerite[ligne][colonne] = caractere.equals(" ") ? ""
				: convertir(Proposition.sEnJeu().elementAt(ordreColonnesDeVerite.elementAt(m++) + Atome.sExplicites().size() - 1).getTableDeVerite()[ligne]);
			}
		}
		int n = 0;
		for (int colonne = 0; colonne < expression.length(); colonne++) {
			String caractere = Character.toString(expression.charAt(colonne));
			valeursDeVerite[nombreLignes_tableDeVerite(nombreDeMasse())][colonne] = caractere.equals(" ") ? ""
			: String.valueOf(ordreColonnesDeVerite.elementAt(n++));
		}
		return valeursDeVerite;
	}

	// convertir (conversion des valeurs "true" et "false" en "1" et "0") :
	public static String convertir(boolean valeur) {
		return valeur ? "1" : "0";
	}

	// calculer_ordreColonnesDeVerite (calcul de l'ordre dans lequel se fait le calcul de la table...) :
	public static Vector<Integer> calculer_ordreColonnesDeVerite() {
		Vector<Integer> ordreColonnesDeVerite = new Vector<>();
		Vector<Integer> sousOrdre = new Vector<>();
		int j = Proposition.sEnJeu().size() - Atome.sExplicites().size();
		ordreColonnesDeVerite.addElement(j);
		for (int ligne = table.size() - 1; ligne > -1; ligne--) {
			for (int colonne = table.elementAt(ligne).size() - 1; colonne > -1; colonne--) {
				String sousProposition = table.elementAt(ligne).elementAt(colonne);
				Pattern motif = Pattern.compile("p\\[\\d+\\.\\d+\\]");
				Matcher correspondant = motif.matcher(sousProposition);
				sousOrdre.removeAllElements();
				while (correspondant.find())
					for (int i = Proposition.sEnJeu().size(); i > 0; i--)
						if (correspondant.group().equals(Proposition.sEnJeu().elementAt(i - 1).getNom()))
							sousOrdre.addElement(i - Atome.sExplicites().size());
				if (sousOrdre.size() > 1) {
					ordreColonnesDeVerite.insertElementAt(sousOrdre.elementAt(0), ordreColonnesDeVerite.indexOf(j));
					ordreColonnesDeVerite.insertElementAt(sousOrdre.elementAt(1), ordreColonnesDeVerite.indexOf(j) + 1);
				}
				else
					ordreColonnesDeVerite.insertElementAt(sousOrdre.elementAt(0), ordreColonnesDeVerite.indexOf(j) + 1);
				j--;
			}
		}
		for (int i = 0; i < ordreColonnesDeVerite.size(); i++)
			if (ordreColonnesDeVerite.elementAt(i) < 1)
				ordreColonnesDeVerite.removeElementAt(i);
		return ordreColonnesDeVerite;
	}
}