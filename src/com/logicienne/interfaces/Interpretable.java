package com.logicienne.interfaces;
/*
 * Nom de l'interface : Interpretable
 *
 * Description		  : Cette interface rend une (sous-)expression interpr�table � partir d'un connecteur...
 *
 * Version			  : 1.0
 *
 * Cr�ation			  : du 21/09/2015 au 21/10/2015
 *
 * Copyright		  : Cyril Marilier
 */
public interface Interpretable
{
	public void interpreted(byte nombreLignesDeLaTableDeVerite, String expression, int degreDeComplexite, int position);
}