package com.logicienne.interfaces;
/*
 * Nom de l'interface : Instantiable
 *
 * Description		  : Cette interface permet, � un atome, d'�tre interpr�t�.
 *
 * Version			  : 1.0
 *
 * Cr�ation			  : du 21/09/2015 au 21/10/2015
 *
 * Copyright		  : Cyril Marilier
 */
public interface Instantiable
{
	public void instantiated(byte nombreLignesDeLaTableDeVerite, byte numeroAtomique);
}