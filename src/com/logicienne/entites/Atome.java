package com.logicienne.entites;
/*
 * Nom de la classe : Atome
 *
 * Version			: 1.0
 *
 * Cr�ation			: du 21/09/2015 au 21/10/2015
 *
 * Copyright		: Cyril Marilier
 */
import java.util.Vector;

import com.logicienne.interfaces.Instantiable;

public class Atome extends Proposition implements Instantiable
{
	// D�claration de la variable (de classe) "sExplicites" :
	private static Vector<Atome> sExplicites = new Vector<>();

	// Constructeur :
	public Atome(String nom) {
		super(nom);
		sExplicites.add(this);
	}

	// Accesseur (en lecture) � l'ensemble des atomes explicites :
	public static Vector<Atome> sExplicites() {
		return sExplicites;
	}

	// M�thode impl�ment�e...
	public void instantiated(byte nombreLignes_TableVerite, byte numeroAtomique) {
		boolean[] valeursVerite = new boolean[nombreLignes_TableVerite];
		byte nombreDePas = (byte)(valeursVerite.length/(byte)Math.pow(2, numeroAtomique));
		byte pas = 0;
		byte etape = nombreDePas;
		boolean valeurDeVerite = true;
		while (etape <= valeursVerite.length) {
			valeurDeVerite = !valeurDeVerite;
			for (; pas < etape; pas++)
				valeursVerite[pas] = valeurDeVerite;
			etape += nombreDePas;
		}
		setTableDeVerite(valeursVerite);
	}
}