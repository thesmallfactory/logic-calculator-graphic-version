package com.logicienne.entites;
/*
 * Nom de la classe : ConnecteurUnaire
 *
 * Version			: 1.0
 *
 * Cr�ation			: du 21/09/2015 au 21/10/2015
 *
 * Copyright		: Cyril Marilier
 */
public class ConnecteurUnaire extends Connecteur
{
	// D�claration des constantes de classe "PRE_SIGNE" + "POST_SIGNE" :
	private static final String PRE_SIGNE = "";
	private static final String POST_SIGNE = "p\\[\\d+\\.\\d+\\]";

	// Constructeur :
	public ConnecteurUnaire(String signe) {
		super(PRE_SIGNE, signe, POST_SIGNE);
	}

	// M�thode impl�ment�e...
	public void interpreted(byte nombreLignesDeLaTableDeVerite, String expression, int degreDeComplexite, int position) {
		if (expression.contains(signe + "p")) {
			boolean[] truthTable = new boolean[nombreLignesDeLaTableDeVerite];
			String appellationSous_expression = expression.substring(1);
			for (int i = 0; i < Proposition.sEnJeu().size(); i++)
				if (appellationSous_expression.equals(Proposition.sEnJeu().elementAt(i).getNom())) {
					if (signe.equals("�"))
						for (int line = 0; line < nombreLignesDeLaTableDeVerite; line++)
							truthTable[line] = !Proposition.sEnJeu().elementAt(i).getTableDeVerite()[line];
					new Proposition("p[" + (degreDeComplexite+1) + "." + (position+1) + "]", truthTable);
				}
		}
	}
}