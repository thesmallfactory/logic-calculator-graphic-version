package com.logicienne.entites;
/*
 * Nom de la classe : Connecteur
 *
 * Version			: 1.0
 *
 * Création			: du 21/09/2015 au 21/10/2015
 *
 * Copyright		: Cyril Marilier
 */
import java.util.Vector;

import com.logicienne.interfaces.Interpretable;

public abstract class Connecteur implements Interpretable
{
	// Déclaration des variable et constantes d'instance :
	protected final String PRE_SIGNE;
	protected String signe;
	protected final String POST_SIGNE;

	// Déclaration de la variable (de classe) "sEnJeu" :
	public static Vector<Connecteur> sEnJeu = new Vector<Connecteur>();

	// Constructeur :
	public Connecteur(String preSigne, String signe, String postSigne) {
		this.PRE_SIGNE = preSigne;
		this.signe = signe;
		this.POST_SIGNE = postSigne;
		sEnJeu.add(this);
	}

	// Accesseur en lecture au "signe" du connecteur :
	public String getSigne() {
		return signe;
	}

	// Redéfinition de la méthode "toString()" :
	public String toString() {
		String regexConnecteur = PRE_SIGNE + signe + POST_SIGNE;
		return regexConnecteur;
	}
}