package com.logicienne.entites;
/*
 * Nom de la classe : ConnecteurBinaire
 *
 * Version			: 1.0
 *
 * Cr�ation			: du 21/09/2015 au 21/10/2015
 *
 * Copyright		: Cyril Marilier
 */
public class ConnecteurBinaire extends Connecteur
{
	// D�claration des constantes de classe "PRE_SIGNE" + "POST_SIGNE" :
	private static final String PRE_SIGNE = "\\(p\\[\\d+\\.\\d+\\][";
	private static final String POST_SIGNE = "]p\\[\\d+\\.\\d+\\]\\)";

	// Connecteur :
	public ConnecteurBinaire(String signe) {
		super(PRE_SIGNE, signe, POST_SIGNE);
	}

	// M�thode impl�ment�e...
	public void interpreted(byte nombreLignesDeLaTableDeVerite, String expression, int degreDeComplexite, int position) {
		if (expression.contains(signe + "p")) {
			boolean[] tableau_Gauche = new boolean[nombreLignesDeLaTableDeVerite];
			boolean[] tableau_Droite = new boolean[nombreLignesDeLaTableDeVerite];
			boolean[] truthTable = new boolean[nombreLignesDeLaTableDeVerite];
			String appellationSousExpression_Gauche = expression.substring(1, expression.indexOf(signe + "p"));
			String appellationSousExpression_Droite = expression.substring(expression.indexOf(signe + "p")+1, expression.indexOf(")"));
			for (int i = 0; i < Proposition.sEnJeu().size(); i++) {
				if (appellationSousExpression_Gauche.equals(Proposition.sEnJeu().elementAt(i).getNom()))
					tableau_Gauche = Proposition.sEnJeu().elementAt(i).getTableDeVerite().clone();
				if (appellationSousExpression_Droite.equals(Proposition.sEnJeu().elementAt(i).getNom()))
					tableau_Droite = Proposition.sEnJeu().elementAt(i).getTableDeVerite().clone();
			}
			if (signe.equals("&"))
				for (int line = 0; line < nombreLignesDeLaTableDeVerite; line++)
					truthTable[line] = tableau_Gauche[line] && tableau_Droite[line];
			if (signe.equals("+"))
				for (int line = 0; line < nombreLignesDeLaTableDeVerite; line++)
					truthTable[line] = tableau_Gauche[line] || tableau_Droite[line];
			new Proposition("p[" + (degreDeComplexite+1) + "." + (position+1) + "]", truthTable);
		}
	}
}