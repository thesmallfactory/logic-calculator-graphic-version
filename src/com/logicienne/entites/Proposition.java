package com.logicienne.entites;
/*
 * Nom de la classe : Proposition
 *
 * Version			: 1.0
 *
 * Cr�ation			: du 21/09/2015 au 21/10/2015
 *
 * Copyright		: Cyril Marilier
 */
import java.util.Vector;

public class Proposition
{
	// D�claration des variables d'instance :
	protected String nom = "";
	protected boolean[] tableDeVerite;

	// D�claration de la variable (de classe) "sEnJeu" :
	private static Vector<Proposition> sEnJeu = new Vector<>();

	// Constructeurs :
	public Proposition(String nom) {
		this.nom = nom;
		sEnJeu.add(this);
	}

	public Proposition(String nom, boolean[] tableDeVerite) {
		this.nom = nom;
		this.tableDeVerite = tableDeVerite;
		sEnJeu.add(this);
	}

	// Accesseur (en lecture, seulement) au "nom" de la proposition :
	public String getNom() {
		return nom;
	}

	// Accesseurs � la "table de v�rit�" de la proposition :
	public boolean[] getTableDeVerite() {
		return tableDeVerite;
	}

	public void setTableDeVerite(boolean[] tableDeVerite) {
		this.tableDeVerite = tableDeVerite;
	}

	// Accesseur (en lecture) � l'ensemble des propositions en jeu :
	public static Vector<Proposition> sEnJeu() {
		return sEnJeu;
	}

	// Red�finition de la m�thode "toString()" :
	public String toString() {
		String affichageProposition = nom + " = [";
		for (boolean valeurDeVerite : tableDeVerite)
			affichageProposition += valeurDeVerite ? "1" : "0";
		return affichageProposition + "]";
	}
}