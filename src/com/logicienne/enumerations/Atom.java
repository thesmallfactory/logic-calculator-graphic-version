package com.logicienne.enumerations;
/*
 * Nom de la classe : Atom
 *
 * Description		: Il s'agit d'une classe "enum�rant" les "atomes"...
 *
 * Version			: 1.0
 *
 * Cr�ation			: du 21/09/2015 au 21/10/2015
 *
 * Copyright		: Cyril Marilier
 */
public enum Atom
{
	// Contenu de l'�num�ration...
	P("p"),
	Q("q"),
	R("r"),
	S("s"),
	T("t");

	// D�claration de la variable (d'instance) "lettre" :
	private String lettre;

	// Constructeur :
	private Atom(String lettre) {
		this.lettre = lettre;
	}

	// Accesseur en lecture (le constructeur s'opposant �l'accesseur en �criture...) � la "lettre" de l'atome :
	public String getLettre() {
		return lettre;
	}
}