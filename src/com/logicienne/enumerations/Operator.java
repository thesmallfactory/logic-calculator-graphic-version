package com.logicienne.enumerations;
/*
 * Nom de la classe : Operator
 *
 * Description		: Il s'agit d'une classe "enum�rant" les "connecteurs"...
 *
 * Version			: 1.0
 *
 * Cr�ation			: du 21/09/2015 au 21/10/2015
 *
 * Copyright		: Cyril Marilier
 */
public enum Operator
{
	// Contenu de l'�num�ration...
	NEGATION("�"),
	CONJONCTION("&"),
	DISJONCTION_INCLUSIVE("+");

	// D�claration de la variable (d'instance) "signe" :
	private String signe;

	// Constructeur :
	private Operator(String signe) {
		this.signe = signe ;
	}

	// Accesseur en lecture (le constructeur s'opposant �l'accesseur en �criture...) au "signe" du connecteur :
	public String getSigne() {
		return signe;
	}
}